//
//  MainView.swift
//  utm
//
//  Created by zvhir on 30/01/2023.
//

import SwiftUI

struct MainView: View {
    
    let primary = UIColor(rgb: 0x222631)
    let secondary = UIColor(rgb: 0x14181f)
    let accent = UIColor(rgb: 0x07efff)
    
    // Example data
    let exampleData = [
        MissionData(title: "DRONOSUTMOPID-0001", subtitle: "Pipeline Visual Inspection Petronas Penapisan Melaka",
                    latitude: 2.184844, longitude: 102.259945),
        MissionData(title: "DRONOSUTMOPID-0002", subtitle: "Drone Visual Mapping for Malaysia Liquified Natural Gas Complex Bintulu", latitude: 3.200585, longitude: 113.043313),
        MissionData(title: "DRONOSUTMOPID-0003", subtitle: "Tank Visual Inspection & Plant Mapping PPTSB Kerteh", latitude: 4.587598, longitude: 103.439160),
        MissionData(title: "DRONOSUTMOPID-0004", subtitle: "Pipeline Visual Inspection PPMSB Plant Demo", latitude: 2.924393, longitude: 101.691243)
    ]
    
    
    init() {
        UITabBar.appearance().unselectedItemTintColor = UIColor.gray
    }
    
    var body: some View {
        TabView {
            DroneView()
                .tabItem {
                    Image(systemName: "sleep.circle.fill")
                    Text("Drone")
                }
            FlightView()
                .tabItem {
                    Image(systemName: "airplane")
                    Text("Flights")
                }
            MissionList(data: exampleData)
                .tabItem {
                    Image(systemName: "plus.app")
                    Text("Mission")
                }
            AnimationView(request: URLRequest(url: URL(string: "https://event-player.vertikaliti.com/zahir.html")!))
                .tabItem {
                    Image(systemName: "globe.asia.australia.fill")
                    Text("Maps")
                    
                }
            ProfileView()
                .tabItem {
                    Image(systemName: "person.fill")
                    Text("Profile")
                }
        }
        .accentColor(Color(accent))
        .onAppear() {
            UITabBar.appearance().backgroundColor = primary
        }
        .background(Color.black)
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
