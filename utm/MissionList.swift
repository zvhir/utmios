//
//  MissionList.swift
//  utm
//
//  Created by zvhir on 07/02/2023.
//

import SwiftUI


// Define a custom data structure
struct MissionData: Identifiable {
    var id = UUID()
    var title: String
    var subtitle: String
    var latitude: Double
    var longitude: Double
}


// Create a custom View for the card
struct CardView: View {
    
    
    var data: MissionData
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text(data.title)
                    .font(.headline)
                Text(data.subtitle)
                    .font(.subheadline)
            }
        }
        .padding()
    }
}

// Create a ViewController
struct MissionList: View {
    
    let primary = UIColor(rgb: 0x222631)
    let secondary = UIColor(rgb: 0x14181f)
    let accent = UIColor(rgb: 0x07efff)
    
    var data: [MissionData]
    var body: some View {
        VStack(alignment: .center) {
            NavigationView {
                List(data) { item in
                    NavigationLink(destination: MapBoxMapView(data: item)) {
                        CardView(data: item)
                    }
                    .listRowBackground(Color(primary))
                    .foregroundColor(Color.white)
                    .navigationBarBackButtonHidden(true)
                }
                .padding(.top, 15)
                .toolbar {
                    ToolbarItem(placement: .principal) {
                        HStack {
                            Text("Missions")
                                .font(.headline)
                                .foregroundColor(.white)
                        }
                    }
                }
                .navigationBarBackButtonHidden(false)
                .background(Color(primary))
                .navigationBarTitleDisplayMode(.inline)
                .navigationBarItems(trailing:
                                        NavigationLink(destination: CreateMission()) {
                    Image(systemName: "plus")
                        .font(.headline)
                        .foregroundColor(.white)
                }
                )
                
                
                
                
            }
        }
    }
    
    init(data: [MissionData]) {
        self.data = data
    }
}
//
//struct MissionList_Previews:
//
//    PreviewProvider {
//    static var previews: some View {
//        MissionList(data: exampleData)
//    }
//}
