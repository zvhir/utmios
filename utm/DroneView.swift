//
//  DroneView.swift
//  utm
//
//  Created by zvhir on 30/01/2023.
//

import SwiftUI

struct DroneView: View {
    
    let primary = UIColor(rgb: 0x222631)
    let secondary = UIColor(rgb: 0x14181f)
    let accent = UIColor(rgb: 0x07efff)
    
    var body: some View {
            ZStack {
                Color(secondary)
                Text("Drone Connect")
                    .foregroundColor(.white)
            }.ignoresSafeArea()
        }
}

struct DroneView_Previews: PreviewProvider {
    static var previews: some View {
        DroneView()
    }
}
