//
//  CreateMission.swift
//  utm
//
//  Created by zvhir on 08/02/2023.
//

import SwiftUI
import MapboxMaps
import CoreLocation
import UIKit
import Foundation

struct CreateMission: UIViewControllerRepresentable {
    
    func makeUIViewController(context: Context) -> CreateMissionController {
        return CreateMissionController()
    }
    
    func updateUIViewController(_ uiViewController: CreateMissionController, context: Context) {
    }
}


class CreateMissionController: UIViewController {
    
    internal var mapView: MapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let myResourceOptions = ResourceOptions(accessToken: "sk.eyJ1IjoienZoaXI5OCIsImEiOiJjbDd6dGd3eDYwY28wM3JwNDZmZWRtMXRnIn0.sqLvd5eYGBqL4zO0I6Y8GA")
        
        let centerCoordinate = CLLocationCoordinate2D(latitude: 2.922623, longitude: 101.641709)
        let point = Point(centerCoordinate)
        
        let mapOptions = MapInitOptions(resourceOptions: myResourceOptions, cameraOptions: CameraOptions(center: centerCoordinate, zoom: 12), styleURI: StyleURI(rawValue: "mapbox://styles/zvhir98/cldvbzt37005h01qmngdx5lcy"))
        
        mapView = MapView(frame: view.bounds, mapInitOptions: mapOptions)
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        view.addSubview(mapView)
        
        // Add the marker and view annotation when the map loads
        mapView.mapboxMap.onNext(event: .mapLoaded) { [weak self] _ in
            guard let self = self else { return }
            let markerId = self.addMarker(at: point)
            self.addViewAnnotation(at: centerCoordinate, withMarkerId: markerId)
        }
    }
    
    // Add a marker image via a symbol layer
    private func addMarker(at point: Point) -> String {
        let featureId = "some-feature-id"
        let style = mapView.mapboxMap.style
        
        // Add an image of a red marker to the style
        try? style.addImage(UIImage(named: "RedPin")!, id: "red-pin-image", stretchX: [], stretchY: [])
        
        // Create a GeoJSON source
        var source = GeoJSONSource()
        let sourceId = "some-source-id"
        var feature = Feature(geometry: Geometry.point(point))
        feature.identifier = .string(featureId)
        source.data = .featureCollection(FeatureCollection(features: [feature]))
        try? mapView.mapboxMap.style.addSource(source, id: sourceId)
        
        // Create a symbol layer using the GeoJSON source
        // and image added above
        var layer = SymbolLayer(id: "some-layer-id")
        layer.source = sourceId
        layer.iconImage = .constant(.name("red-pin-image"))
        layer.iconAnchor = .constant(.bottom)
        layer.iconAllowOverlap = .constant(false)
        try? mapView.mapboxMap.style.addLayer(layer)
        
        return featureId
    }
    
    // Update options to attach to a feature
    private func addViewAnnotation(at coordinate: CLLocationCoordinate2D, withMarkerId markerId: String? = nil) {
        let options = ViewAnnotationOptions(
            geometry: Point(coordinate),
            width: 100,
            height: 40,
            associatedFeatureId: markerId,
            anchor: .bottom,
            offsetY: 40
        )
        let sampleView = createSampleView(withText: "119 meter")
        try? mapView.viewAnnotations.add(sampleView, options: options)
    }
    
    // Use the same sample view defined in the previous section
    private func createSampleView(withText text: String) -> UIView {
        let label = UILabel()
        label.text = text
        label.font = .systemFont(ofSize: 14)
        label.numberOfLines = 0
        label.textColor = .black
        label.backgroundColor = .white
        label.textAlignment = .center
        return label
    }
}
