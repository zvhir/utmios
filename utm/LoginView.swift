//
//  LoginView.swift
//  utm
//
//  Created by zvhir on 30/01/2023.
//

import SwiftUI

//struct LoginView: View {
//
//    @Binding var isLogged: Bool
//
//    var body: some View {
//
//        Button(action: {
//            isLogged = true
//        }, label: {
//            Text("Login")
//        })
//    }
//}


struct LoginView: View {
    
    @Binding var isLogged: Bool
    
    let primary = UIColor(rgb: 0x222631)
    let secondary = UIColor(rgb: 0x14181f)
    let accent = UIColor(rgb: 0x07efff)
    
    @State private var email: String = ""
    @State private var password: String = ""
    @State var isPresenting = false

    var body: some View {
        ZStack {
            Color(primary)
                .ignoresSafeArea()
            VStack(alignment: .leading) {
                Spacer()
                Image("AerodyneLogo")
                    .resizable()
                       .frame(width: 60.0, height: 60.0)
                       .padding(.bottom,20)
                Text("Welcome to Aerodyne UTM")
                    .foregroundColor(.white)
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding(.bottom,10)
                    .font(.system(size: 30) .bold())
                Text("Where you can monitor and apply permit in one place")
                    .foregroundColor(.white)
                    .frame(maxWidth: .infinity, alignment: .leading)
                
               
                Spacer()
                TextField("", text: $email)
                .modifier(PlaceholderStyle(showPlaceHolder: email.isEmpty,
                    placeholder: "Email address"))
                .accentColor(Color(accent))
                TextField("", text: $password)
                .modifier(PlaceholderStyle(showPlaceHolder: password.isEmpty,
                    placeholder: "Password"))
                .accentColor(Color(accent))
                .padding(.bottom, 20)

                Button(action: {
                      isLogged = true
                    print("perform login logic")
                    Configuration.value(value: "Zahiruddin Zainal", forKey: "userData")
                  }, label: {
                      HStack(alignment: .center) {
                          Text("Login")
                              .foregroundColor(.black)
                          Image(systemName: "arrow.right")
                      }
                      .padding(.all, 10.0)
                  })
                .frame(maxWidth: .infinity, alignment: .center)
                .background(Color(accent))
                .cornerRadius(5.0)
                
               

                Spacer()
                Text("Forgot password")
                    .foregroundColor(Color(accent))
                    .frame(maxWidth: .infinity, alignment: .center)
                    .font(.system(size: 15))
            }.padding(.all,30)
        }
        .accentColor(Color.black)
    }
}

class Configuration {

    static func value<T>(defaultValue: T, forKey key: String) -> T{

        let preferences = UserDefaults.standard
        return preferences.object(forKey: key) == nil ? defaultValue : preferences.object(forKey: key) as! T
    }

    static func value(value: Any, forKey key: String){

        UserDefaults.standard.set(value, forKey: key)
    }

}

extension UIColor {
   convenience init(red: Int, green: Int, blue: Int) {
       assert(red >= 0 && red <= 255, "Invalid red component")
       assert(green >= 0 && green <= 255, "Invalid green component")
       assert(blue >= 0 && blue <= 255, "Invalid blue component")

       self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
   }

   convenience init(rgb: Int) {
       self.init(
           red: (rgb >> 16) & 0xFF,
           green: (rgb >> 8) & 0xFF,
           blue: rgb & 0xFF
       )
   }
}

public struct PlaceholderStyle: ViewModifier {
    var showPlaceHolder: Bool
    var placeholder: String

    public func body(content: Content) -> some View {
        ZStack(alignment: .leading) {
            if showPlaceHolder {
                Text(placeholder)
                .padding(.vertical, 10)
                .foregroundColor(Color.gray)
            }
            content
            .foregroundColor(Color.white)
            .padding(0.0)
//            .border(Color.teal, width: 1)
        }
    }
}


