//
//  MapView.swift
//  utm
//
//  Created by zvhir on 30/01/2023.
//

import SwiftUI
import WebKit

struct AnimationView : UIViewRepresentable {

    let request: URLRequest

    func makeUIView(context: Context) -> WKWebView  {
        return WKWebView()
    }

    func updateUIView(_ uiView: WKWebView, context: Context) {
        uiView.load(request)
    }

}

#if DEBUG
struct AnimationView_Previews : PreviewProvider {
    static var previews: some View {
        AnimationView(request: URLRequest(url: URL(string: "https://www.apple.com")!))
    }
}
#endif
