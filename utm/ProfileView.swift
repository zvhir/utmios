//
//  ProfileView.swift
//  utm
//
//  Created by zvhir on 30/01/2023.
//

import SwiftUI


struct ProfileView: View {
    
    let primary = UIColor(rgb: 0x222631)
    let secondary = UIColor(rgb: 0x14181f)
    let accent = UIColor(rgb: 0x07efff)
    
    @State private var showingAlert = false
    
    let userData = Configuration.value(defaultValue: "no user data", forKey: "userData")
    
    var body: some View {
            ZStack {
                Color(secondary)
                VStack(alignment: .center) {
                    Spacer()
                    Image("Avatar")
                        .resizable()
                        .frame(width: 60, height: 60)
                        .clipShape(Circle())
                        .padding(.bottom, 20)
                    Text(userData)
                        .foregroundColor(.white)
                        .padding(.bottom, 5)
                    Button(action: {
                        print("Prompt alert to logout")
                        showingAlert = true
                    }) {
                        HStack {
                            Text("Logout")
                            Image(systemName: "arrow.right")
                        }
                    }
                    .alert(isPresented:$showingAlert) {
                                Alert(
                                    title: Text("Are you sure you want to logout?"),
                                    primaryButton: .destructive(Text("Logout")) {
                                        print("Logging out..")
                                        print(userData)
                                    },
                                    secondaryButton: .cancel()
                                )
                            }
                    Spacer()
                    Spacer()
                }
            }.ignoresSafeArea()
        }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView()
    }
}
