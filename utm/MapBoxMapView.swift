//
//  MapBoxMapView.swift
//  utm
//
//  Created by zvhir on 02/02/2023.
//

import SwiftUI
import MapboxMaps
import CoreLocation
import UIKit
import Foundation

struct MapBoxMapView: UIViewControllerRepresentable {
    
    var data: MissionData
    
    func makeUIViewController(context: Context) -> MapViewController {
        return MapViewController(data: data)
    }
    
    func updateUIViewController(_ uiViewController: MapViewController, context: Context) {
    }
}



@objc(AnimateGeoJSONLine)
public class MapViewController: UIViewController, LocationConsumer {
    
    var data: MissionData!
    
    init(data: MissionData) {
        self.data = data
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func locationUpdate(newLocation: MapboxMaps.Location) {
        // nothing
    }
    
    internal var mapView: MapView!
    
    let primary = UIColor(rgb: 0x222631)
    let secondary = UIColor(rgb: 0x14181f)
    let accent = UIColor(rgb: 0x07efff)
    
    
    internal var isAtStart = true
    var instuctionLabel = UILabel(frame: CGRect.zero)
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        let backButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        backButton.tintColor = .red
        navigationItem.backBarButtonItem = backButton
    
        mapView = MapView(frame: view.bounds, mapInitOptions: .init(styleURI: .satelliteStreets))
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.mapboxMap.setCamera(to: .init(center: CLLocationCoordinate2D(latitude: 40, longitude: -78), zoom: 1.0))
        try! self.mapView.mapboxMap.style.setProjection(StyleProjection(name: .globe))
        
        mapView.mapboxMap.onNext(event: .styleLoaded) { _ in
            try! self.mapView.mapboxMap.style.setAtmosphere(Atmosphere())
            self.addTerrain()
            //                self.finish()
        }
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(animateCameraOnClick))
        mapView.addGestureRecognizer(tap)
        
        instuctionLabel.text = data.title
        instuctionLabel.textColor = accent
        instuctionLabel.textAlignment = .center
        instuctionLabel.layer.backgroundColor = primary.cgColor
        instuctionLabel.layer.cornerRadius = 10.0
        instuctionLabel.font = UIFont.systemFont(ofSize: 10)
        instuctionLabel.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(mapView)
        view.addSubview(instuctionLabel)
        installConstraints()
    }
 
    
    func installConstraints() {
        let safeView = view.safeAreaLayoutGuide
        
        NSLayoutConstraint.activate([
            instuctionLabel.topAnchor.constraint(equalTo: safeView.bottomAnchor, constant: -70),
            instuctionLabel.bottomAnchor.constraint(equalTo: safeView.bottomAnchor, constant: -30),
            instuctionLabel.leadingAnchor.constraint(equalTo: safeView.leadingAnchor, constant: 70),
            instuctionLabel.trailingAnchor.constraint(equalTo: safeView.trailingAnchor, constant: -70)
        ])
        
    }
    
    func addTerrain() {
        var demSource = RasterDemSource()
        demSource.url = "mapbox://mapbox.mapbox-terrain-dem-v1"
        // Setting the `tileSize` to 514 provides better performance and adds padding around the outside
        // of the tiles.
        demSource.tileSize = 514
        demSource.maxzoom = 14.0
        try! mapView.mapboxMap.style.addSource(demSource, id: "mapbox-dem")
        
        var terrain = Terrain(sourceId: "mapbox-dem")
        terrain.exaggeration = .constant(1.5)
        
        try! mapView.mapboxMap.style.setTerrain(terrain)
    }
    
    @objc func animateCameraOnClick() {
        instuctionLabel.isHidden = true
        var target = CameraOptions()
        
        var cameraStart = CameraOptions(
            center: CLLocationCoordinate2D(latitude: 36, longitude: 80),
            zoom: 1.0,
            bearing: 0,
            pitch: 0)
        
        var cameraEnd = CameraOptions(
            center: CLLocationCoordinate2D(latitude: data.latitude, longitude:  data.longitude),
            zoom: 17,
            bearing: 130.0,
            pitch: 75.0)
        
        if isAtStart {
            target = cameraEnd
        } else {
            target = cameraStart
        }
        isAtStart = !isAtStart
        mapView.camera.fly(to: target, duration: 12)
        
    }
}
