//
//  utmApp.swift
//  utm
//
//  Created by zvhir on 30/01/2023.
//

import SwiftUI

@main
struct utmApp: App {
    
    @State private var isLogged = false
    let myValue = Configuration.value(defaultValue: "default_value", forKey: "key_1")
    
    var body: some Scene {
        WindowGroup {
           
            if isLogged {
                MainView()
            } else {
                LoginView(isLogged: $isLogged)
            }
        }
    }
}

