//
//  FlightView.swift
//  utm
//
//  Created by zvhir on 30/01/2023.
//

import SwiftUI

struct FlightView: View {
    
    let primary = UIColor(rgb: 0x222631)
    let secondary = UIColor(rgb: 0x14181f)
    let accent = UIColor(rgb: 0x07efff)
    
    var body: some View {
            ZStack {
                Color(secondary)
                Text("Flights around you")
                    .foregroundColor(.white)
            }.ignoresSafeArea()
        }
}

struct FlightView_Previews: PreviewProvider {
    static var previews: some View {
        FlightView()
    }
}
